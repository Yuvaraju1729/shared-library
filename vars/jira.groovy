def call(String msg="STAGE FAILED")
{
 
 /* jiraAddComment comment: msg, idOrKey:id , site: 'jira' */
 def issue = [fields: [ project: [key:'SB'],
                       summary: 'BUG created',
                       description: '${msg}',
                       issuetype: [name: 'Bug']]]
def newIssue = jiraNewIssue issue: issue, site: 'Jira'
echo newIssue.data.key

 }
